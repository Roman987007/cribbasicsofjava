package com.example.roman.cribjava;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


public class NavigationViewActivity extends AppCompatActivity {

    public String fileName = "mainFile.html";
    ExpandableListAdapter mMenuAdapter;
    ExpandableListView expandableList;
    WebView webView;
    String titleAB;
    String pathHtml;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    private DrawerLayout mDrawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_view);

        webView = findViewById(R.id.simpleWebView);
        webView.loadUrl("file:///android_asset/" + fileName);
//включаем поддержку масштабирования
        webView.getSettings().setBuiltInZoomControls(true);

//полосы прокрутки – внутри изображения, увеличение места для просмотра
        webView.setScrollbarFadingEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        final ActionBar ab = getSupportActionBar();
        assert ab != null;
        ab.setHomeAsUpIndicator(R.drawable.menu);
        ab.setDisplayHomeAsUpEnabled(true);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        expandableList = findViewById(R.id.navigationmenu);
        NavigationView navigationView = findViewById(R.id.nav_view);


        if (navigationView != null) {

            // Настроить view drawer'а
            setupDrawerContent(navigationView);
        }


        //Подготавливаем список данных:
        prepareListData();
        mMenuAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);

        // Настраиваем listAdapter
        expandableList.setAdapter(mMenuAdapter);

        expandableList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousItem = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition != previousItem)
                    expandableList.collapseGroup(previousItem);
                previousItem = groupPosition;
            }
        });

        expandableList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {

                String[] directions = getResources().getStringArray(R.array.parentsDirectory);
                String[] namesHtml1, namesHtml2, namesHtml3, namesHtml4, namesHtml5;
                String[] array11, array12, array13, arrayl4, array15;


                String[][] nameFile = {
                        namesHtml1 = getResources().getStringArray(R.array.nameFile1),
                        namesHtml2 = getResources().getStringArray(R.array.nameFile2),
                        namesHtml3 = getResources().getStringArray(R.array.nameFile3),
                        namesHtml4 = getResources().getStringArray(R.array.nameFile4),
                        namesHtml5 = getResources().getStringArray(R.array.nameFile5)};
                String[][] titleHeaders = {
                        array11 = getResources().getStringArray(R.array.parent1),
                        array12 = getResources().getStringArray(R.array.parent2),
                        array13 = getResources().getStringArray(R.array.parent3),
                        arrayl4 = getResources().getStringArray(R.array.parent4),
                        array15 = getResources().getStringArray(R.array.parent5)
                };
                titleAB = titleHeaders[i][i1];
                setTitle(titleHeaders[i][i1]);
                pathHtml = directions[i] + nameFile[i][i1];
                webView.loadUrl(directions[i] + nameFile[i][i1]);

                mDrawerLayout.closeDrawer(GravityCompat.START);
                return false;
            }
        });
        expandableList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                switch (i) {
                    case 2:
                        setTitle("Типы данных");
                        webView.loadUrl("file:///android_asset/JavaDataTypes/DataType.html");
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                        break;
                }
                return false;
            }
        });
    }


    private void prepareListData() {
        //Добавляем данные о пунктах списка:
        listDataHeader = new ArrayList<>();
        String[] parentHeaders = getResources().getStringArray(R.array.itemParents);
        List<String> item = new ArrayList<String>(Arrays.asList(parentHeaders));
        listDataHeader.addAll(item);

        listDataChild = new HashMap<>();
        int[] parentArray = {R.array.parent1, R.array.parent2, R.array.parent3, R.array.parent4, R.array.parent5};
        //Добавляем данные о подпунктах:
        for (int i = 0; i <= listDataHeader.size(); i++) {
            List<String> childList = new ArrayList<>();
            String[] child = getResources().getStringArray(parentArray[i]);
            List<String> childHeaders = new ArrayList<>(Arrays.asList(child));
            childList.addAll(childHeaders);
            listDataChild.put(listDataHeader.get(i), childList);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        //revision: this don't works, use setOnChildClickListener() and setOnGroupClickListener() above instead
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        return true;
                    }
                });
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey("titleAB") && savedInstanceState.containsKey("webView")) {
            titleAB = savedInstanceState.getString("titleAB");
            pathHtml = savedInstanceState.getString("webView");
            setTitle(titleAB);
            webView.loadUrl(pathHtml);
        }
    }


    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("titleAB", titleAB);
        outState.putString("webView", pathHtml);
    }

}
